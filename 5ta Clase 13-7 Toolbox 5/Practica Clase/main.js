//Declaracion de array
let autos = []

//Funcion de iteracion (muestra a usuario la lista)
function iterador(){
    let listado = ""
    for (let i = 0; i < autos.length; i++) {
        listado = listado + "<li>" + autos[i] + "</li>"    
        document.getElementById("lista-autos").innerHTML = listado
    }
}

//Funcion para agregar
document.getElementById("agregar").addEventListener("click", function(){

    let marca = document.getElementById("marca-auto").value
    autos.push(marca)
    iterador()
    for (let index = 0; index < autos.length; index++) {
        console.log(autos[index])
        
    }
})

//Funcion para eliminar el ultimo
document.getElementById("eliminar2").addEventListener("click", function(){
    autos.pop()
    iterador()
    console.log(autos.length)
    
})

//Funcion para eliminar el primero
document.getElementById("eliminar1").addEventListener("click", function(){
    autos.shift()
    iterador()
})

