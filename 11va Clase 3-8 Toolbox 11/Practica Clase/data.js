const arrayMovil = [
    {
        marca: 'samsung',
        gama: 'media',
        modelo: 'a10',
        pantalla: 'Fullhd',
        so: 'android 9.1',
        precio: 10000
    },
    {
        marca: 'lg',
        gama: 'baja',
        modelo: 'lumius',
        pantalla: 'hd',
        so: 'android 10.1',
        precio: 5000
    },
    {
        marca: 'redmi',
        gama: 'alta',
        modelo: 'note 20',
        pantalla: '8k',
        so: 'android 15.5',
        precio: 20000
    }
    ,
    {
        marca: 'samsung',
        gama: 'alta',
        modelo: 's30',
        pantalla: '8k',
        so: 'android 14',
        precio: 15000
    }
    
] ;

module.exports = arrayMovil;