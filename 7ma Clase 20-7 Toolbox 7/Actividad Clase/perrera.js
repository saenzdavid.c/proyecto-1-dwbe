let estadoAdopcion = ["En adopcion","En proceso de adopción","Adoptado"];
let perrera = [];
let perro1 = new Perro ("Pepe", "Pug", "Macho", 5, "Chico", estadoAdopcion[0])
let perro2 = new Perro ("Vacuna", "Golden", "Hembra", 8, "Grande", estadoAdopcion[0])
let perro3 = new Perro ("Sony", "Beagle", "Macho", 3, "Chico", estadoAdopcion[0])

perrera.push(perro1,perro2,perro3);                                         // Creo array Perrera y meto los perros de ejemplo
listarPerros(perrera);                                                      // Cargo la lista pre definida

function cargarPerro(){
    let carga = true;
    while(carga){
        let nombre = prompt("Ingrese el nombre del perro");
        while (!nombre){
            nombre = prompt("No puede dejar el campo vacio")}
        let raza = prompt("Ingrese la raza del perro");
        while (!raza){
            raza = prompt("No puede dejar el campo vacio")}    
        let sexo = prompt("Ingrese el sexo del perro");
        while (!sexo){
            sexo = prompt("No puede dejar el campo vacio")}         
        let edad = prompt("Ingrese la edad del perro");
        while (!edad){
            edad =  prompt("No puede dejar el campo vacio")}
        let tamanyo = prompt("Ingrese el tamaño del perro");
        while (!tamanyo){
            tamanyo = prompt("No puede dejar el campo vacio")}
        let perroIngresado = new Perro (nombre,raza,sexo,edad,tamanyo,estadoAdopcion[0]);   // Creo un nuevo elemento Perro con los valores ingresados
        perrera.push(perroIngresado);                                                       // Lo meto en el array perrera
        listarPerros(perrera);                                                              // listo en pantalla todos los perros
        carga = window.confirm("¿Desea cargar otro perro?");
    }        
}

function modificarPerro(){
    let idPerro = prompt("Ingrese el ID del Perro solicitado");
    while(!verificarPerro(idPerro)){                                                    // Llamo a la funcion que verifica el ID de un perro existente
        idPerro = prompt("El ID no fue encontrado");
    }
    let nuevoEstado = prompt("Ingrese el nuevo estado del perro: \n1:En Adopcion \n2:En proceso de adopcion \n3:Adoptado");
    while(!verificarEstado(nuevoEstado-1)){                                             // Verifico que se ingrese uno de los 3 estados correctos.
        nuevoEstado = prompt("Ingrese un estado correcto:  \n1:En Adopcion \n2:En proceso de adopcion \n3:Adoptado")
    }
    perrera[idPerro].setEstado(estadoAdopcion[nuevoEstado-1]);                          // Segun ID de perro, seteo el estado de adopcion con el valor ingresado anteriormente
                                                                                        // menos 1, ya que el array comienza con indice 0. La opcion 3 de Adoptado pertenece al indice 2 
    listarPerros(perrera);                                                              // del array estadoAdopcion. Luego vuelvo a listar los perros.
}

////////////////////////////////////////////////////////////////////////
function verificarPerro(idPerro){                                                       // Entro con el ID ingresado en la funcion mofidicarPerro()
    if (idPerro != ""){
        if (idPerro < perrera.length && idPerro >= 0){                                  // y verifico que sea distinto de " " y pertenezca a un valor del array Perrera.
            return true;
        }
        else{
            return false;
        }
    }
}
////////////////////////////////////////////////////////////////////////
function verificarEstado(nuevoEstado){                                                  // Verifico que se ingrese uno de los 3 estados correctos.
    if (nuevoEstado >= 0 && nuevoEstado < 3){
        return true;
    }else{
        return false;
    }
}
////////////////////////////////////////////////////////////////////////
function listarPerros(filtrados){
    let userHTML = "";
    filtrados.forEach(function(perro, indice) {
        userHTML += `<p><b>[Perro ID ${indice}] Nombre: </b>${perro.nombre}, <b>Raza:</b> ${perro.raza}, <b>Sexo:</b> ${perro.sexo}, <b>Edad:</b> ${perro.edad} años, <b>Tamaño:</b> ${perro.tamanyo}, <b>Estado:</b> ${perro.estadoAdopcion}</p>`;
    });                                       
    document.getElementById("listarPerros").innerHTML = userHTML;                       // Envio lo armado antes al HTML
}
////////////////////////////////////////////////////////////////////////
function filtrarEstado(nuevoEstado){                                                    // Funcion que filtra el estado del perro
    if (nuevoEstado == "5"){                                                            // La opcion "5" borra todos los filtros, muestra toda la perrera
    listarPerros(perrera);
    }else{
    filtroAdoptado = perrera.filter(perro => perro.estadoAdopcion == estadoAdopcion[nuevoEstado]);
    listarPerros(filtroAdoptado);                                                       // Vuelve a listarPerros con los arrays filtrados
    }
}